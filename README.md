# kortdev's PHPCS Configuration 

Use this package to set up a consistent PHPCS configuration across Laravel projects. 

If you wish to use this configuration in your project, install it using composer:
Download the ruleset.xml, rename it to phpcs.xml and place it in your root.

If you wish to use this configuration in global envirioment, install it globally using composer:
`composer global require --dev kortdev/laravel-phpcs`

And set it to default standard:
`phpcs --config-set default_standard kortdev-phpcs`

--

Afterwards, you may run `php vendor/bin/phpcs -i` and see `kortdev-phpcs` listed as an installed ruleset.

You may now use the ruleset like so:
For one file: `phpcs --standard=kortdev-phpcs my_file.php`
For the whole directory: `phpcd . --standard=kortdev-phpcs`

Additional arguments:
`-e` - List rules & standards
`-n` - Hide warnings
`-l` - Local directory online
`--report=code` - Report foreach codeline
`--report=gitblame` - Report for gitblame
`--report=source` - Report for source

_Note_, If you're installing globally with Composer you can simply do `phpcs -i`.
